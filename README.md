# GenerationDisfluence

Il y a trois notebook :
- notebook/generateur-lstm.ipynb : modèle sans tuner
- notebook/generateur-lstm_tuner_lr.ipynb : modèle avce tuner qui effectue une recherche pour le learning rate
- notebook/generateur-lstm_tuner_lstm.ipynb : modèle avce tuner qui effectue une recherche pour la taille du lstm

***

## Name
Génération de disfluence

## Description
L'objectif était de créer un modèle de génération de texte oral qui inclut des disfluences et des marqueurs discursifs.
